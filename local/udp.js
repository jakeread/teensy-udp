import dgram from 'dgram'
import performance from 'perf_hooks'

let timestamp = () => {
  return performance.performance.now()
}

// ahn dgram (node's UDP object) socket 
const sckt = dgram.createSocket('udp4')

sckt.on('error', (err) => {
  console.log(`sckt error:\n${err.stack}`)
  sckt.close()
})

sckt.on('message', (msg, rinfo) => {
  count ++
  if(count > iter){
    let time = timestamp() - startTime
    console.log(`averate rtt ${time / iter}`)
  } else {
    txPck()
  }
})

sckt.on('listening', () => {
  const addr = sckt.address();
  console.log(`sckt listening on ${addr.address}:${addr.port}`)
  // send something (?) 
  setTimeout(() => {
    console.log(`begin...`)
    startTime = timestamp()
    txPck()
  }, 1000)
})

sckt.bind(8040);

let txPck = () => {
  return new Promise((resolve, reject) => {
    sckt.send("hello-udp", 8888, "130.44.148.111", (cb) => {
      resolve()
    })
  })
}

let startTime = 0 
let count = 0
let iter = 1000 
