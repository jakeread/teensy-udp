#include <Arduino.h>
#include <NativeEthernet.h>
#include <NativeEthernetUdp.h>

// coordinates 
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192, 168, 1, 177);
unsigned int localPort = 8888;

// todo: teensys have a MAC written into the fuse, s/o paul, u r the goat 
// this forum post: https://forum.pjrc.com/threads/62932-Teensy-4-1-MAC-Address
// suggests we can retrieve it with 
void teensyMAC(uint8_t *mac) {
    for(uint8_t by=0; by<2; by++) mac[by]=(HW_OCOTP_MAC1 >> ((1-by)*8)) & 0xFF;
    for(uint8_t by=0; by<4; by++) mac[by+2]=(HW_OCOTP_MAC0 >> ((3-by)*8)) & 0xFF;
    Serial.printf("MAC: %02x:%02x:%02x:%02x:%02x:%02x\n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}

// buffers 
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];
char replyBuffer[] = "acknowledged";

// instance 
EthernetUDP UDP;

void setup() {
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  // setup debug line 
  Serial.begin(9600);
  while(!Serial); // waits until serial connection established 
  Serial.println("hello serial");

  //try DHCSP 
  if(Ethernet.begin(mac) == 0){
    Serial.println("DHCP failed");
  } else {
    UDP.begin(localPort);
    Serial.print("alive on: ");
    Serial.print(Ethernet.localIP());
    Serial.print(":");
    Serial.println(localPort);
  }
}

unsigned long lastTick = 0;

void loop() {
  // read packets if we can 
  int pckLen = UDP.parsePacket();
  if(pckLen){
    //Serial.print("rx'd pck w/ len ");
    //Serial.println(pckLen);
    // lettuce reply 
    UDP.beginPacket(UDP.remoteIP(), UDP.remotePort());
    UDP.write(replyBuffer);
    UDP.endPacket();
  }

  if(millis() > lastTick + 250){
    lastTick = millis();
    digitalWrite(13, !digitalRead(13));
  }
}